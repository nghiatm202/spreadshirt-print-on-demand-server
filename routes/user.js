const router = require("express").Router();
const controller = require("../controllers/user");
const { isAdmin, verifyAccessToken } = require("../middlewares/verifyToken");

router.post("/register", controller.register);
router.post("/login", controller.login);
router.get("/me", [verifyAccessToken], controller.getMe);
router.post("/refresh-token", controller.refreshToken);
router.get("/logout", controller.logout);
router.get("/forgot-password", controller.forgotPassword);
router.put("/reset-password", controller.resetPassword);
router.get("/", [verifyAccessToken, isAdmin], controller.getUserList);
router.delete("/", [verifyAccessToken, isAdmin], controller.deleteUser);
router.put("/me", [verifyAccessToken], controller.updateUser);
router.put("/address", [verifyAccessToken], controller.updateUserAddress);
router.put(
  "/:userId",
  [verifyAccessToken, isAdmin],
  controller.updateUserByAdmin
);

module.exports = router;
