const router = require("express").Router();
const controller = require("../controllers/order");
const { isAdmin, verifyAccessToken } = require("../middlewares/verifyToken");

// Route for admin
router.put("/:orderId", [verifyAccessToken, isAdmin], controller.updateStatus);
router.get("/admin", [verifyAccessToken, isAdmin], controller.getOrderList);

// Route for user
router.post("/", [verifyAccessToken], controller.createOrder);
router.get("/", verifyAccessToken, controller.getUserOrder);
router.get("/:orderId", verifyAccessToken, controller.getOrderById);
router.delete("/:orderId", verifyAccessToken, controller.deleteOrder);

module.exports = router;
