const userRouter = require("./user");
const productRouter = require("./product");
const orderRouter = require("./order");
const topUpRouter = require("./topUp");

const { notFound, errorHandler } = require("../middlewares/errorHandler");

const initRoutes = (app) => {
  app.use("/api/user", userRouter);
  app.use("/api/product", productRouter);
  app.use("/api/order", orderRouter);
  app.use("/api/top-up", topUpRouter);

  app.use(notFound);
  app.use(errorHandler);
};

module.exports = initRoutes;
