const router = require("express").Router();
const controller = require("../controllers/product");
const { isAdmin, verifyAccessToken } = require("../middlewares/verifyToken");
const uploadCloud = require("../config/cloudinary");

router.post("/", [verifyAccessToken, isAdmin], controller.createProduct);
router.get("/", controller.getProductList);

router.put(
  "/upload-images/:productId",
  [verifyAccessToken, isAdmin],
  uploadCloud.array("images"),
  controller.uploadProductImages
);

router.put(
  "/:productId",
  [verifyAccessToken, isAdmin],
  controller.updateProduct
);
router.delete(
  "/:productId",
  [verifyAccessToken, isAdmin],
  controller.deleteProduct
);
router.get("/:productId", controller.getProduct);

module.exports = router;
