const router = require("express").Router();
const controller = require("../controllers/topUp");
const { isAdmin, verifyAccessToken } = require("../middlewares/verifyToken");

// Route for admin
router.put("/:topUpId", [verifyAccessToken, isAdmin], controller.updateStatus);
router.put(
  "/:topUpId/approve",
  [verifyAccessToken, isAdmin],
  controller.approveTopUp
);
router.get("/admin", [verifyAccessToken, isAdmin], controller.getTopUpList);

// Route for user
router.post("/", [verifyAccessToken], controller.createTopUp);
router.get("/", verifyAccessToken, controller.getUserTopUp);
router.get("/:topUpId", verifyAccessToken, controller.getTopUpById);

module.exports = router;
