const Order = require("../models/order");
const Product = require("../models/product");
const asyncHandler = require("express-async-handler");

const calculateTotalPrice = (order) => {
  let totalPrice = 0;

  for (const item of order.products) {
    const product = item.product;
    const variant = item.variant;
    const printLocations = item.printLocations || [];

    let itemPrice = product.basePrice || 0;

    if (variant) {
      itemPrice += variant.price || 0;
    }

    itemPrice += product.printPrice * printLocations.length;
    itemPrice += product.additionalPrice;

    for (let i = 1; i < item.quantity; i++) {
      itemPrice += product.additionalPrice;
    }

    totalPrice += itemPrice * item.quantity;
  }

  return totalPrice;
};

const createOrder = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { products } = req.body;

  if (!products || products.length === 0) {
    throw new Error("No products provided.");
  }

  const processedProducts = await Promise.all(
    products.map(async (item) => {
      const product = await Product.findById(item.productId);

      if (!product) {
        throw new Error(`Product with ID ${item.productId} not found.`);
      }

      const variant = product.variants.find(
        (variant) => variant._id.toString() === item.variantId.toString()
      );

      if (!variant) {
        throw new Error(
          `Variant with ID ${item.variantId} not found for product ${item.productId}.`
        );
      }

      return {
        product: product,
        color: item.color,
        variant: variant,
        quantity: item.quantity || 1,
      };
    })
  );

  const totalPrice = calculateTotalPrice({
    products: processedProducts,
  });

  const response = await Order.create({
    user: _id,
    products: processedProducts,
    totalPrice,
  });

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to create a new order.",
  });
});

const getOrderList = asyncHandler(async (req, res) => {
  try {
    const queries = { ...req.query };
    const specialFields = ["limit", "sort", "page", "fields"];
    specialFields.forEach((field) => delete queries[field]);

    let queryString = JSON.stringify(queries);

    queryString = queryString.replace(
      /\b(gte|gt|lt|lte)\b/g,
      (matchedQuery) => `$${matchedQuery}`
    );

    const formattedQueries = JSON.parse(queryString);

    let queryCommand = Order.find(formattedQueries);

    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      queryCommand = queryCommand.sort(sortBy);
    }

    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      queryCommand = queryCommand.select(fields);
    }

    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;
    const skip = (page - 1) * limit;
    queryCommand.skip(skip).limit(limit);

    const response = await queryCommand.exec();
    const total = await Order.find(formattedQueries).countDocuments();

    return res.status(200).json({
      success: response ? true : false,
      total,
      result: response ? response : "Failed to retrieve orders.",
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, result: error.message });
  }
});

const updateStatus = asyncHandler(async (req, res) => {
  const { orderId } = req.params;
  const { status } = req.body;

  if (!status) throw new Error("Please provide all required information.");

  const response = await Order.findByIdAndUpdate(
    orderId,
    { status },
    { new: true }
  );

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to update the status.",
  });
});

const getUserOrder = asyncHandler(async (req, res) => {
  const { _id } = req.user;

  const response = await Order.find({ user: _id });

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to retrieve user orders.",
  });
});

const getOrderById = asyncHandler(async (req, res) => {
  const { orderId } = req.params;

  const response = await Order.findById(orderId);

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to retrieve user orders.",
  });
});

const deleteOrder = asyncHandler(async (req, res) => {
  const { orderId } = req.params;

  const response = await Order.findByIdAndDelete(orderId);

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to retrieve user orders.",
  });
});

module.exports = {
  getOrderList,
  createOrder,
  updateStatus,
  getUserOrder,
  getOrderById,
  deleteOrder,
};
