const TopUp = require("../models/topUp");
const User = require("../models/user");
const asyncHandler = require("express-async-handler");

const createTopUp = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { status, ...topUpData } = req.body;

  const response = await TopUp.create({
    user: _id,
    status: "pending",
    ...topUpData,
  });

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to create a new top-up.",
  });
});

const getTopUpById = asyncHandler(async (req, res) => {
  const { topUpId } = req.params;

  const response = await TopUp.findById(topUpId);

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to retrieve user top-up.",
  });
});

const updateStatus = asyncHandler(async (req, res) => {
  const { topUpId } = req.params;
  const { status } = req.body;

  if (!status) throw new Error("Please provide all required information.");

  const response = await TopUp.findByIdAndUpdate(
    topUpId,
    { status },
    { new: true }
  );

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to update the status.",
  });
});

const getTopUpList = asyncHandler(async (req, res) => {
  try {
    const queries = { ...req.query };
    const specialFields = ["limit", "sort", "page", "fields"];
    specialFields.forEach((field) => delete queries[field]);

    let queryString = JSON.stringify(queries);

    queryString = queryString.replace(
      /\b(gte|gt|lt|lte)\b/g,
      (matchedQuery) => `$${matchedQuery}`
    );

    const formattedQueries = JSON.parse(queryString);

    let queryCommand = TopUp.find(formattedQueries);

    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      queryCommand = queryCommand.sort(sortBy);
    }

    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      queryCommand = queryCommand.select(fields);
    }

    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;
    const skip = (page - 1) * limit;
    queryCommand.skip(skip).limit(limit);

    const response = await queryCommand.exec();
    const total = await TopUp.find(formattedQueries).countDocuments();

    return res.status(200).json({
      success: response ? true : false,
      total,
      result: response ? response : "Failed to retrieve top-up.",
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, result: error.message });
  }
});

const approveTopUp = asyncHandler(async (req, res) => {
  const { topUpId } = req.params;
  const { amount } = req.body;

  const topUp = await TopUp.findById(topUpId);
  if (!topUp) {
    return res.status(404).json({ error: "Top-up not found." });
  }

  const user = await User.findByIdAndUpdate(
    topUp.user,
    { virtualMoney: amount },
    { new: true }
  );

  if (!user) {
    return res.status(404).json({ error: "User not found." });
  }

  topUp.status = "approved";
  await topUp.save();

  return res.json({
    success: user ? true : false,
    result: user ? user : "Failed to approve top-up.",
  });
});

const getUserTopUp = asyncHandler(async (req, res) => {
  const { _id } = req.user;

  const response = await TopUp.find({ user: _id });

  return res.json({
    success: response ? true : false,
    result: response ? response : "Failed to retrieve user top-up.",
  });
});

module.exports = {
  createTopUp,
  getTopUpById,
  updateStatus,
  getTopUpList,
  approveTopUp,
  getUserTopUp,
};
