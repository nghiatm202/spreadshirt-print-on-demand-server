const User = require("../models/user");
const asyncHandler = require("express-async-handler");
const { generateAccessToken } = require("../middlewares/jwt");
const jwt = require("jsonwebtoken");
const sendEmail = require("../utils/sendEmail");
const crypto = require("crypto");

const register = asyncHandler(async (req, res) => {
  const { email, password, firstName, lastName } = req.body;

  if (!email || !password || !firstName || !lastName) {
    return res.status(400).json({
      success: false,
      message: "Please provide all required information.",
    });
  }

  const user = await User.findOne({ email });

  if (user) {
    throw new Error(
      "This email address is already associated with an existing account."
    );
  } else {
    const newUser = await User.create(req.body);

    return res.status(200).json({
      success: newUser ? true : false,
      message: newUser
        ? "Registration successful."
        : "An error occurred during registration. Please try again later.",
    });
  }
});

const login = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({
      success: false,
      message: "Please provide all required information.",
    });
  }

  const response = await User.findOne({ email });

  if (response && (await response.isCorrectPassword(password))) {
    const { _id, password, role, refreshToken, ...userData } =
      response.toObject();
    const accessToken = generateAccessToken(_id, role);
    const newRefreshToken = generateAccessToken(_id);

    await User.findByIdAndUpdate(
      _id,
      { refreshToken: newRefreshToken },
      { new: true }
    );

    res.cookie("refreshToken", newRefreshToken, {
      httpOnly: true,
      maxAge: 7 * 24 * 60 * 60 * 1000,
    });

    return res.status(200).json({
      success: true,
      accessToken,
      userData,
    });
  } else {
    throw new Error("Invalid credentials.");
  }
});

const getMe = asyncHandler(async (req, res) => {
  const { _id } = req.user;

  const user = await User.findById(_id).select("-password -role -refreshToken");

  return res.status(200).json({
    success: user ? true : false,
    result: user ? user : "User not found.",
  });
});

const refreshToken = asyncHandler(async (req, res, next) => {
  const cookies = req.cookies;

  if (!cookies && !cookies.refreshToken)
    throw new Error("No refresh token found in cookies.");

  const result = jwt.verify(cookies.refreshToken, process.env.JWT_SECRET_KEY);

  const response = await User.findOne({
    _id: result._id,
    refreshToken: cookies.refreshToken,
  });

  return res.status(200).json({
    success: response ? true : false,
    newAccessToken: response
      ? generateAccessToken(response._id, response.role)
      : "Refresh token does not match.",
  });
});

const logout = asyncHandler(async (req, res) => {
  const cookies = req.cookies;

  if (!cookies && !cookies.refreshToken)
    throw new Error("No refresh token found in cookies.");

  await User.findOneAndUpdate(
    { refreshToken: cookies.refreshToken },
    { refreshToken: "" },
    { new: true }
  );

  res.clearCookie("refreshToken", {
    httpOnly: true,
    secure: true,
  });

  return res.status(200).json({
    success: true,
    message: "Logout successful.",
  });
});

// Luong reset password:
// Client gui email.
// Server check email co hop le hay khong => Gui email + kem theo link (password reset token).
// Client check mail + click link.
// Client call api kem theo token.
// Check token co giong voi token ma server gui mail hay khong.
// Change password.

const forgotPassword = asyncHandler(async (req, res) => {
  const { email } = req.query;
  if (!email) throw new Error("Email is missing.");

  const user = await User.findOne({ email });
  if (!user) throw new Error("User not found.");

  const resetPasswordToken = user.generateResetPasswordToken();
  await user.save();

  const resetLink = `${process.env.SERVER_URL}/api/user/reset-password/${resetPasswordToken}`;

  const result = await sendEmail(email, resetLink);

  return res.status(200).json({
    success: true,
    result,
  });
});

const resetPassword = asyncHandler(async (req, res) => {
  const { password, token } = req.body;

  if (!password || !token) throw new Error("Missing password or token.");

  const resetPasswordToken = crypto
    .createHash("sha256")
    .update(token)
    .digest("hex");

  const user = await User.findOne({
    passwordResetToken: resetPasswordToken,
    passwordResetExpiryTime: { $gt: Date.now() },
  });

  if (!user) throw new Error("Invalid reset token.");

  user.password = password;
  user.passwordResetToken = undefined;
  user.passwordChangedAt = Date.now();
  user.passwordResetExpiryTime = undefined;

  await user.save();

  return res.status(200).json({
    success: user ? true : false,
    message: user
      ? "Password updated successfully."
      : "Something went wrong during password update.",
  });
});

const getUserList = asyncHandler(async (req, res) => {
  const response = await User.find().select("-password -role -refreshToken");

  return res.status(200).json({
    success: response ? true : false,
    users: response,
  });
});

const deleteUser = asyncHandler(async (req, res) => {
  const { _id } = req.query;
  if (!_id) throw new Error("Missing user ID.");
  const response = await User.findByIdAndDelete(_id).select(
    "-password -role -refreshToken"
  );

  return res.status(200).json({
    success: response ? true : false,
    message: response
      ? "User successfully deleted."
      : "No user found or deleted.",
  });
});

const updateUser = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  if (!_id || Object.keys(req.body).length === 0)
    throw new Error("Please provide all required information.");

  if (req.body.role) {
    throw new Error("Updating user role is not allowed.");
  }

  const response = await User.findByIdAndUpdate(_id, req.body, {
    new: true,
  }).select("-password -role -refreshToken");

  return res.status(200).json({
    success: response ? true : false,
    result: response
      ? response
      : "Unable to update user. Please check your input.",
  });
});

const updateUserByAdmin = asyncHandler(async (req, res) => {
  const { userId } = req.params;
  if (Object.keys(req.body).length === 0)
    throw new Error("Please provide all required information.");

  const response = await User.findByIdAndUpdate(userId, req.body, {
    new: true,
  }).select("-password -role -refreshToken");

  return res.status(200).json({
    success: response ? true : false,
    result: response
      ? response
      : "Unable to update user. Please check your input.",
  });
});

const updateUserAddress = asyncHandler(async (req, res) => {
  const { _id } = req.user;

  if (Object.keys(req.body.address).length === 0)
    throw new Error("Please provide all required information.");

  const response = await User.findByIdAndUpdate(
    _id,
    {
      $push: {
        address: req.body.address,
      },
    },
    {
      new: true,
    }
  ).select("-password -role -refreshToken");

  return res.status(200).json({
    success: response ? true : false,
    result: response
      ? response
      : "Unable to update user address. Please check your input.",
  });
});

module.exports = {
  register,
  login,
  getMe,
  refreshToken,
  logout,
  forgotPassword,
  resetPassword,
  getUserList,
  deleteUser,
  updateUser,
  updateUserByAdmin,
  updateUserAddress,
};
