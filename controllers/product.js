const Product = require("../models/product");
const asyncHandler = require("express-async-handler");
const slugify = require("slugify");

const createProduct = asyncHandler(async (req, res) => {
  if (Object.keys(req.body).length === 0)
    throw new Error("Please provide all required information.");

  if (req.body && req.body.name) req.body.slug = slugify(req.body.name);
  const newProduct = await Product.create(req.body);

  return res.status(200).json({
    success: newProduct ? true : false,
    result: newProduct ? newProduct : "Failed to create a new product.",
  });
});

const getProduct = asyncHandler(async (req, res) => {
  const { productId } = req.params;

  const product = await Product.findById(productId);

  return res.status(200).json({
    success: product ? true : false,
    result: product ? product : "Failed to retrieve the product.",
  });
});

const getProductList = asyncHandler(async (req, res) => {
  try {
    const queries = { ...req.query };
    const specialFields = ["limit", "sort", "page", "fields"];
    specialFields.forEach((field) => delete queries[field]);

    let queryString = JSON.stringify(queries);

    queryString = queryString.replace(
      /\b(gte|gt|lt|lte)\b/g,
      (matchedQuery) => `$${matchedQuery}`
    );

    const formattedQueries = JSON.parse(queryString);

    if (queries?.name)
      formattedQueries.name = { $regex: queries.name, $options: "i" };

    let queryCommand = Product.find(formattedQueries);

    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      queryCommand = queryCommand.sort(sortBy);
    }

    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      queryCommand = queryCommand.select(fields);
    }

    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;
    const skip = (page - 1) * limit;
    queryCommand.skip(skip).limit(limit);

    const response = await queryCommand.exec();
    const total = await Product.find(formattedQueries).countDocuments();

    return res.status(200).json({
      success: response ? true : false,
      total,
      result: response ? response : "Failed to retrieve products.",
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ success: false, result: error.message });
  }
});

const updateProduct = asyncHandler(async (req, res) => {
  const { productId } = req.params;
  if (req.body && req.body.name) req.body.slug = slugify(req.body.name);
  const updatedProduct = await Product.findByIdAndUpdate(productId, req.body, {
    new: true,
  });

  return res.status(200).json({
    success: updatedProduct ? true : false,
    result: updatedProduct ? updatedProduct : "Failed to update the product.",
  });
});

const deleteProduct = asyncHandler(async (req, res) => {
  const { productId } = req.params;
  const deletedProduct = await Product.findByIdAndDelete(productId);

  return res.status(200).json({
    success: deletedProduct ? true : false,
    result: deletedProduct ? deletedProduct : "Failed to delete the product.",
  });
});

const uploadProductImages = asyncHandler(async (req, res) => {
  const { productId } = req.params;
  if (!req.files) throw new Error("Please provide all required information.");

  const response = await Product.findByIdAndUpdate(
    productId,
    {
      $push: { images: { $each: req.files.map((item) => item.path) } },
    },
    { new: true }
  );

  return res.status(200).json({
    success: response ? true : false,
    result: response ? response : "Failed to upload the product image.",
  });
});

module.exports = {
  createProduct,
  getProduct,
  getProductList,
  updateProduct,
  deleteProduct,
  uploadProductImages,
};
