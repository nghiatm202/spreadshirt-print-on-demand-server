const nodemailer = require("nodemailer");
const asyncHandler = require("express-async-handler");

const sendEmail = asyncHandler(async (email, resetLink) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_HOST,
      pass: process.env.EMAIL_HOST_PASSWORD,
    },
  });

  const info = await transporter.sendMail({
    from: '"Fred Foo 👻" <no-reply@example.com>',
    to: email,
    subject: "Password Reset Request",
    text: `Dear user,\n\nYou have requested a password reset. Please click on the following link to reset your password: ${resetLink}`,
    html: `<p>Dear user,</p>
    <p>You have requested a password reset. Please click on the following link to reset your password:</p>
    <p><a href="${resetLink}">${resetLink}</a></p>`,
  });

  return info;
});

module.exports = sendEmail;
