const { default: mongoose } = require("mongoose");

const dbConnect = async () => {
  try {
    const connection = await mongoose.connect(process.env.MONGO_URI);
    if (connection.connection.readyState === 1) {
      console.log("Database connection successful.");
    } else {
      console.log("Database connection in progress.");
    }
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = dbConnect;
