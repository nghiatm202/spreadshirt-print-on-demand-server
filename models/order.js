const mongoose = require("mongoose");

const printLocationSchema = new mongoose.Schema({
  position: {
    type: String,
    required: true,
    enum: ["front", "back", "armsRight", "armsLeft", "neck"],
  },
  designUrl: {
    type: String,
    required: true,
  },
});

const orderSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    products: [
      {
        product: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Product",
          required: true,
        },
        color: {
          type: String,
          required: true,
        },
        variant: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "ProductVariant",
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
          default: 1,
        },
        printLocations: [printLocationSchema],
      },
    ],
    status: {
      type: String,
      enum: ["on-hold", "ready", "production", "fulfilled", "canceled"],
      default: "on-hold",
    },
    paymentStatus: {
      type: String,
      enum: ["paid", "unpaid", "cancelled"],
      default: "unpaid",
    },
    totalPrice: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Order = mongoose.model("Order", orderSchema);

module.exports = Order;
