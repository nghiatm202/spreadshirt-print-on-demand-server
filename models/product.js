const mongoose = require("mongoose");
const ProductVariant = require("./productVariant");

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
    },
    description: {
      type: String,
      required: true,
    },
    images: {
      type: Array,
    },
    colors: {
      type: [String],
      required: true,
    },
    basePrice: {
      type: Number,
      required: true,
      default: 0,
    },
    printPrice: {
      type: Number,
      required: true,
      default: 0,
    },
    additionalPrice: {
      type: Number,
      required: true,
      default: 0,
    },
    tags: [String],
    variants: [ProductVariant.schema],
  },
  {
    timestamps: true,
  }
);

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
